function onOpen() {
  SpreadsheetApp.getUi().createAddonMenu().addItem('Download file', 'menuDownloadFile').addToUi();
}

function menuDownloadFile(){
  var htmlTemplate = HtmlService.createTemplateFromFile('dialog');
  var htmlOutput = htmlTemplate.evaluate();
  SpreadsheetApp.getUi().showModalDialog(htmlOutput, 'Download dialog');
}

function downloadFile(){
  var values = SpreadsheetApp.getActiveSheet().getDataRange().getDisplayValues();
  var htmlTemplate = HtmlService.createTemplateFromFile('template');
  htmlTemplate.content = JSON.stringify(values, null, '  ');
  
  var content = htmlTemplate.evaluate().getContent();
  var file = DriveApp.createFile(Utilities.formatString('%s.html', new Date().getTime()), content, 'text/html');
  var downloadUrl = file.getDownloadUrl();
  return downloadUrl;
}